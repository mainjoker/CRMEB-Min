<?php

namespace app\swagger\route;

use think\facade\Route;


Route::get('getapi/api', 'Index/getApiContent')->append(['api_name' => 'api']);
Route::get('getapi/adminapi', 'Index/getApiContent')->append(['api_name' => 'adminapi']);
Route::get('getapi/kefuapi', 'Index/getApiContent')->append(['api_name' => 'kefuapi']);
Route::get('index', 'Index/index');
Route::get('/', 'Index/index');
Route::get('/:name', 'Index/index');


